package designer.subprocesses;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class FactB implements java.io.Serializable
{

   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Position(value = 0)
   private java.lang.Integer b;

   public java.lang.Integer getB()
   {
      return this.b;
   }

   public void setB(java.lang.Integer b)
   {
      this.b = b;
   }

   public FactB()
   {
   }

   public FactB(java.lang.Integer b)
   {
      this.b = b;
   }

}